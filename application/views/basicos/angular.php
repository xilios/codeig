<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplo Angular</title>
</head>
<body ng-app="myApp" ng-controller="myController">
	
	<h1>
    Angular
  </h1>
	<a class="btn btn-primary" href="<?php echo base_url("/curso/basicos/"); ?>" role="button">Volver</a>
	<br/><br/>
	<div>
		
		Ingrese nombre:
		<input type="text" name="nombre" id="nombre" ng-model="name"></input>
	</div>
	<div id="output">Hola {{name}}</div>
</body>
<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="<?php echo base_url("/assets/js/basicos/angular.js"); ?>" rel="stylesheet"></script>
</html>